**Words sampler**

A single parsing function that check out words within a null termating C-string.

It registers, for each word found, the adress of the first char and replace the first trailing space with a terminating '\0' character.

