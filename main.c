/*-
 * Words sampler -License-Identifier: BSD-2-Clause
 * Copyright (c) 2019, David Marec
 *All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>


/* fields addresses */
#define FIELD_DB 100


/*
   Parse a string to extract fields delimited by spaces
   This function writes the terminating '\0' character
   at the end of each field found and stores the
   starting character address into the field array

   It stops at the end of string or when the maximum fields size
   is reached.

   @line: line to parse
   @fields: list of string fields, to be allocated first
   @max: maximum fields to assign

   return field assigned counter
   */

size_t parse(char* line, char* fields[],size_t max)
{
	enum state {SPACE,DATA} s; /* state machine */
	char* cursor=line; /* starting point */
	char* datacursor=line; /* starting first field */
	size_t field_index=0; /* field index*/
	int is_space; /* character delim */
	s=SPACE;

	while(*cursor!=0 && (field_index<max)){

		switch (s){
			case SPACE:

				if(!isspace(*cursor))
				{
					/* 
					   no more spaces
					   register the starts of the field
					   */
					datacursor=cursor;
					fields[field_index]=datacursor;
					++field_index;
					/* forward to next space */
					s=DATA;
				}
				break;
			case DATA:
				if(isspace(*cursor))
				{
					/* end of this field */
					*cursor=0;
					/* forward to next delim character */
					s=SPACE;
				}
				break;
			default:
				break;
		}

		/* next char */
		++cursor;

	}

	return field_index;
}

void usage(const char* myname)
{
	printf("Usage:\n");
	printf("%s <fields> <string>\n",myname);
	printf("\tfields: maximum field to assign (1..%d).\n",FIELD_DB);
	printf("\tstring: long string with space.\n");
	exit(1);
}

int main(int argc,char* argv[])
{
	
	int index_max;
	if(argc!=3){
		usage(argv[0]);
	}
	index_max=atoi(argv[1]);	


	if((index_max<1) || (index_max>FIELD_DB)){
		usage(argv[0]);
	}

	char** myfields=calloc(sizeof(char*),index_max);
	printf("ready to parse: <%s> in order to fuel %d fields.\n",argv[2],index_max);

	index_max=parse(argv[2],myfields,index_max);

	for(size_t i=0;i<index_max;++i){
		if(myfields[i]!=NULL){
			printf("#%02lu:<%s>\n",i,myfields[i]);
		}
	}

	/* useless finaly */
	free(myfields);

}


